const mongoose = require('mongoose');

const TareaSchema = mongoose.Schema({
    nombre: {
        type: String,
        required: true,
        trim: true
    },
    estado: {
        type: String,
        required: false,
        trim: true
    },
    descripcion: {
        type: String,
        required: true,
        trim: true
    },
    creado: {
        type: Date,
        default: Date.now()
    }, 
    creador: {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Usuario'
    }
});

module.exports = mongoose.model('Tarea', TareaSchema);