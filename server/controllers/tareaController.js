const Tarea = require('../models/Tarea');
const { validationResult } = require('express-validator');

// Crea una nueva tarea
exports.crearTarea = async (req, res) => {

    // Revisar si hay errores
    const errores = validationResult(req);
    if( !errores.isEmpty() ) {
        return res.status(400).json({errores: errores.array() })
    }
    

    try {

        // Extraer el proyecto y comprobar si existe
        const { nombre } = req.body;

        // Revisar si el proyecto actual pertenece al usuario autenticado
        if(!nombre) {
            return res.status(401).json({msg: 'Error ingrese una tarea'});
        }

        // Creamos la tarea
        const tarea = new Tarea(req.body);
        await tarea.save();
        res.json({ tarea });
    
    } catch (error) {
        console.log(error);
        res.status(500).send('Hubo un error')
    }

}

// Obtiene las tareas por proyecto
exports.obtenerTareas = async (req, res) => {

        try {
            // Extraer el y comprobar si existe
           const { tarea } = req.query;


            const existeTarea = await Tarea.findById(tarea);
            if(!existeTarea) {
                return res.status(404).json({msg: 'Tarea no encontrado'})
            }

            // Revisar si el tarea actual pertenece al usuario autenticado
            if(existeTarea.creador.toString() !== req.usuario.id ) {
                
                return res.status(401).json({msg: 'No Autorizado'});
            }

            // Obtener las tareas 
            const tareas = await Tarea.find().sort({ creado: -1 });
            res.json({ tareas });

        } catch (error) {
            console.log(error);
            res.status(500).send('Hubo un error');
        }
}

// Actualizar una tarea
exports.actualizarTarea = async (req, res ) => {
    try {
        // Extraer la tarea y comprobar si existe
        const { nombre, descripcion, estado } = req.body;


        // Si la tarea existe o no
        let tarea = await Tarea.findById(req.params.id);

        console.log(tarea.creador);

        if(!tarea) {
            return res.status(404).json({msg: 'No existe esa tarea'});
        }


        // Revisar si la tarea actual pertenece al usuario autenticado
        if(tarea.creador.toString() !== req.usuario.id ) {
            return res.status(401).json({msg: 'No Autorizado'});
        }
        // Crear un objeto con la nueva información
        const nuevaTarea = {};
        nuevaTarea.nombre = nombre;
        nuevaTarea.descripcion = descripcion;
        nuevaTarea.estado = estado;

        // Guardar la tarea
        tarea = await Tarea.findOneAndUpdate({_id : req.params.id }, nuevaTarea, { new: true } );

        res.json({ tarea });

    } catch (error) {
        console.log(error);
        res.status(500).send('Hubo un error')
    }
}


// Elimina una tarea
exports.eliminarTarea = async (req, res) => {
    try {
        // Extraer la tarea y comprobar si existe
        const { nombre, descripcion, estado } = req.body;


        // Si la tarea existe o no
        let tarea = await Tarea.findById(req.params.id);

        console.log(tarea.creador);

        if(!tarea) {
            return res.status(404).json({msg: 'No existe esa tarea'});
        }


        // Revisar si la tarea actual pertenece al usuario autenticado
        if(tarea.creador.toString() !== req.usuario.id ) {
            return res.status(401).json({msg: 'No Autorizado'});
        }
        // Eliminar
        await Tarea.findOneAndRemove({_id: req.params.id});
        res.json({msg: 'Tarea Eliminada'})

    } catch (error) {
        console.log(error);
        res.status(500).send('Hubo un error')
    }
}